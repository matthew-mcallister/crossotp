import 'dart:math' as math;

import 'package:equatable/equatable.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

/// A `SliverGridDelegate` in which children have a minimum cross axis
/// extent.
///
/// As many children as possible will be placed along the cross axis,
/// and they will expand beyond the minimum extent to fill the space.
/// At least one child is always placed, even if this violates the
/// minimum extent constraint. The children will have a fixed extent
/// on the main axis, as opposed to a fixed aspect ratio.
///
/// To construct a grid using this delegate, see the `gridView` method.
@immutable
class SliverGridDelegateWithMinCrossAxisExtent extends SliverGridDelegate
  with EquatableMixin
{
  double mainAxisExtent;
  double minCrossAxisExtent;
  double mainAxisSpacing;
  double crossAxisSpacing;

  SliverGridDelegateWithMinCrossAxisExtent({
    this.mainAxisExtent,
    this.minCrossAxisExtent,
    this.mainAxisSpacing,
    this.crossAxisSpacing,
  }) {
    assert(minCrossAxisExtent > 0.0);
    assert(mainAxisExtent > 0.0);
    assert(mainAxisSpacing >= 0.0);
    assert(crossAxisSpacing >= 0.0);
  }

  @override
  List<Object> get props => [
    mainAxisExtent,
    minCrossAxisExtent,
    mainAxisSpacing,
    crossAxisSpacing,
  ];

  @override
  SliverGridLayout getLayout(SliverConstraints constraints) {
    final minChildExt = minCrossAxisExtent;
    final space = crossAxisSpacing;
    final ext = constraints.crossAxisExtent;
    final minStride = minChildExt + space;
    // crossAxisCount is >= 1 and always has room for the final
    // (non-existent) spacing gap.
    final crossAxisCount = math.max(((ext + space) / minStride).floor(), 1);
    final crossAxisExtent = (ext + space) / crossAxisCount - space;
    final reversed = axisDirectionIsReversed(constraints.crossAxisDirection);
    return SliverGridRegularTileLayout(
      crossAxisCount: crossAxisCount,
      mainAxisStride: mainAxisExtent + mainAxisSpacing,
      crossAxisStride: crossAxisExtent + crossAxisSpacing,
      childMainAxisExtent: mainAxisExtent,
      childCrossAxisExtent: crossAxisExtent,
      reverseCrossAxis: reversed,
    );
  }

  @override
  bool shouldRelayout(covariant SliverGridDelegate oldDelegate) {
    return this != oldDelegate;
  }

  static GridView gridView({
    Key key,
    Axis scrollDirection: Axis.vertical,
    bool reverse: false,
    ScrollController controller,
    bool primary,
    ScrollPhysics physics,
    bool shrinkWrap: false,
    EdgeInsetsGeometry padding,
    @required double mainAxisExtent,
    @required double minCrossAxisExtent,
    double mainAxisSpacing: 0.0,
    double crossAxisSpacing: 0.0,
    bool addAutomaticKeepAlives: true,
    bool addRepaintBoundaries: true,
    bool addSemanticIndexes: true,
    double cacheExtent,
    List<Widget> children: const <Widget>[],
    int semanticChildCount,
    DragStartBehavior dragStartBehavior: DragStartBehavior.start,
    ScrollViewKeyboardDismissBehavior keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.manual,
    String restorationId,
    Clip clipBehavior: Clip.hardEdge
  }) => GridView(
    key: key,
    scrollDirection: scrollDirection,
    reverse: reverse,
    controller: controller,
    primary: primary,
    physics: physics,
    shrinkWrap: shrinkWrap,
    padding: padding,
    gridDelegate: SliverGridDelegateWithMinCrossAxisExtent(
      mainAxisExtent: mainAxisExtent,
      minCrossAxisExtent: minCrossAxisExtent,
      mainAxisSpacing: mainAxisSpacing,
      crossAxisSpacing: crossAxisSpacing,
    ),
    addAutomaticKeepAlives: addAutomaticKeepAlives,
    addRepaintBoundaries: addRepaintBoundaries,
    addSemanticIndexes: addSemanticIndexes,
    cacheExtent: cacheExtent,
    children: children,
    semanticChildCount: semanticChildCount,
    dragStartBehavior: dragStartBehavior,
    clipBehavior: clipBehavior,
    keyboardDismissBehavior: keyboardDismissBehavior,
    restorationId: restorationId,
  );
}
