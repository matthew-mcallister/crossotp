import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'grid.dart';

void main() => runApp(OtpApp());

class Const {
  static const String title = 'CrossOTP';
}

@immutable
class OtpCardData {
  String code;
  String account;
  IconData icon;

  OtpCardData(this.code, this.account, this.icon);
}

class OtpApp extends StatefulWidget {
  @override
  _OtpAppState createState() => _OtpAppState();
}

class _OtpAppState extends State<StatefulWidget> {
  List<OtpCardData> cards;

  _OtpAppState() {
    cards = [
      OtpCardData('123456', 'john.doe@gmail.com', Icons.mail),
      OtpCardData('789012', 'foo@baz.com', Icons.voicemail),
      OtpCardData('345678', 'fop@bim.com', Icons.vpn_key),
      OtpCardData('901234', 'john.jacob.jingleheimer@yahoo.com', Icons.wallet_giftcard),
      OtpCardData('567890', 'quux@bofur.com', Icons.wb_cloudy),
    ];
  }

  // Will compute codes from OTP account data.
  List<OtpCardData> getCards() => cards;

  void shuffle() {
    setState(() {
      cards.insert(0, cards.removeLast());
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: Const.title,
      theme: ThemeData(
        primaryColor: Colors.green,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text(Const.title),
          actions: [
            IconButton(
              icon: Icon(Icons.shuffle),
              onPressed: () { shuffle(); },
            ),
          ],
        ),
        body: OtpCardView(getCards()),
      ),
    );
  }
}

class OtpCardView extends StatelessWidget {
  List<OtpCardData> cards;

  OtpCardView(this.cards);

  Widget _buildCard(BuildContext context, OtpCardData data) {
    final code = data.code;
    assert(code.length == 6);
    return Card(child: ListTile(
      leading: Icon(data.icon, size: 54),
      title: Text(
        code.substring(0, 3) + ' ' + code.substring(3, 6),
        style: TextStyle(
          color: const Color(0xff666666),
          fontSize: 32,
          fontWeight: FontWeight.bold,
        ),
      ),
      subtitle: Text(
        data.account,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          color: Colors.grey,
          fontSize: 20,
        ),
      ),
      trailing: Icon(Icons.copy, size: 36),
      onTap: () {
        Clipboard.setData(ClipboardData(text: code));
        // TODO: show error when copy failed (harder than it sounds)
        // TODO: prevent duplicate snackbars from queueing up
        // Both TODOs call for a more sophisticated notification system
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Copied to clipboard.'),
        ));
      },
    ));
  }

  Widget _buildCards(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      child: SliverGridDelegateWithMinCrossAxisExtent.gridView(
        padding: const EdgeInsets.all(4),
        mainAxisExtent: 88,
        minCrossAxisExtent: 360,
        mainAxisSpacing: 4,
        crossAxisSpacing: 4,
        children: cards.map((x) => _buildCard(context, x)).toList(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) => _buildCards(context);
}
